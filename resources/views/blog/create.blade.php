@extends('template.index')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper ">
    <!-- Content Header (Page header) -->
    <div class="card">
        <div class="card-header">
            <h5 class="card-title">New Blog</h5>
            <a href="{{url('blog')}}" class="btn btn-sm btn-warning float-right">Kembali</a>
        </div>
        <div class="card-body">
            <form role="form" action="{{url('blog')}}" method="post">
            @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">Judul Blog</label>
                    <input type="text" class="form-control @error('judul') is-invalid @enderror" id="judul" name="judul" value="{{old('judul')}}" placeholder="Judul Blog">
                      @error('judul')
                          <div class="text-danger">{{ $message }}</div>
                      @enderror
                  </div>
                  <div class="form-group">
                    <label for="isi">Isi Blog</label>
                    <textarea type="text" class="form-control @error('isi') is-invalid @enderror" id="isi" name="isi" value="{{old('isi')}}" placeholder="Judul Blog"></textarea>
                      @error('isi')
                          <div class="text-danger">{{ $message }}</div>
                      @enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-sm btn-primary">Simpan</button>
                </div>
              </form>
        </div>
    </div>
  </div>
  <!-- /.content-wrapper -->
@endsection