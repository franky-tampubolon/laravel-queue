@extends('template.index')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper ">
    <!-- Content Header (Page header) -->
    <div class="card">
        <div class="card-header">
            <h5 class="card-title">Daftar User</h5>
            <a href="{{url('user')}}" class="btn btn-sm btn-warning float-right">Kembali</a>
        </div>
        <div class="card-body">
            <form role="form" action="{{url('user')}}" method="post">
            @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Nama User</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" value="{{old('name')}}" id="name" name="name" placeholder="Password">
                      @error('name')
                          <div class="text-danger">{{ $message }}</div>
                      @enderror
                  </div>
                  
                  <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{old('email')}}" placeholder="Enter email">
                      @error('email')
                        <div class="text-danger">{{ $message }}</div>
                      @enderror
                  </div>
                  
                  <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" placeholder="Password">
                      @error('password')
                        <div class="text-danger">{{ $message }}</div>
                      @enderror
                  </div>
                  
                  <div class="form-group">
                    <label for="role">Role User :</label>
                    <select type="text" class="form-control select2" multiple="multiple" name="role[]" id="role">
                        @foreach($role as $role)
                            <option value="{{$role->id}}" {{ old('role') == $role->id ? 'selected' : '' }}>{{$role->role}}</option>                            
                        @endforeach
                    </select>
                      @error('role')
                        <div class="text-danger">{{ $message }}</div>
                      @enderror
                  </div>
                  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-sm btn-primary">Simpan</button>
                </div>
              </form>
        </div>
    </div>
  </div>
  <!-- /.content-wrapper -->
@endsection


@push('cssDatatable')
  <link rel="stylesheet" href="{{asset('adminlte')}}/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="{{asset('adminlte')}}/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <link rel="stylesheet" href="{{asset('adminlte')}}/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
  

@endpush

@push('jsDatatable')
  <script src="{{asset('adminlte')}}/plugins/select2/js/select2.full.min.js"></script>
  <script src="{{asset('adminlte')}}/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
  <script>
  $(document).ready(function() {
    $('.select2').select2();
});
  </script>
@endpush
