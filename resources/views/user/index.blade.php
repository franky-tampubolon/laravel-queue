@extends('template.index')
@include('sweetalert::alert')
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper ">
    <!-- Content Header (Page header) -->
    <div class="card">
        <div class="card-header">
            <h5 class="card-title">Daftar User</h5>
            <a href="{{url('user/create')}}" class="btn btn-sm btn-primary float-right">Tambah User</a> 
        </div>
        <div class="card-body">
            <table id="datatable" class="table table-striped table-hover">
                <thead>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Role Access</th>
                    <th>Aksi</th>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>@foreach($user->roles as $role)
                                {{$role->role}}
                            @endforeach</td>
                            <td>
                                <a href="{{url('user/'.$user->id)}}" class="btn btn-sm btn-success">Edit</a>
                                <a href="#" class="btn btn-sm btn-danger">Hapus</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
  </div>
  <!-- /.content-wrapper -->
@endsection





@push('jsDatatable')
<!-- DataTables -->
<script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endpush

@push('cssDatatable')
<!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">

  @endpush