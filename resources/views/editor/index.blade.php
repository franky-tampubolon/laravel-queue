@extends('template.index')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper ">
    <!-- Content Header (Page header) -->
    <div class="card">
        <div class="card-header">
            <h5>Daftar Blog Belum di Publish</h5>
            <a href="{{url('blog/create')}}" class="btn btn-primary float-right">Buat Blog Baru</a> 
        </div>
        <div class="card-body">
            <table id="datatable" class="table table-striped table-hover">
                <thead>
                    <th>No</th>
                    <th>Judul Blog</th>
                    <th>Isi Blog</th>
                    <th>Aksi</th>
                </thead>
                <tbody>
                    @foreach($blogs as $blog)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$blog->judul}}</td>
                            <td>{{$blog->isi}}</td>
                            <td>
                                <form method="post" action="{{url('editor/update/'.$blog->id)}}">@csrf @method('patch')
                                <button type="submit" class="btn btn-sm btn-success">Update</button>
                                </form>
                                <a href="#" class="btn btn-sm btn-danger">Hapus</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
  </div>
  <!-- /.content-wrapper -->
@endsection





@push('jsDatatable')
<!-- DataTables -->
<script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endpush

@push('cssDatatable')
<!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">

  @endpush