@extends('template.index')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper ">
    <!-- Content Header (Page header) -->
    <div class="card">
        <div class="card-header">
            <h5 class="card-title">Daftar User</h5>
            <a href="{{url('role')}}" class="btn btn-sm btn-warning float-right">Kembali</a>
        </div>
        <div class="card-body">
            <form role="form" action="{{url('role')}}" method="post">
            @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="role">Role User</label>
                    <input type="text" class="form-control @error('role') is-invalid @enderror" id="role" name="role" placeholder="Role User">
                  </div>
                  @error('role')
                      <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-sm btn-primary">Simpan</button>
                </div>
              </form>
        </div>
    </div>
  </div>
  <!-- /.content-wrapper -->
@endsection