@include('template.header')

@include('template.navbar')
@include('template.sidebar')

@yield('content')

@include('sweetalert::alert')
@include('template.footer')