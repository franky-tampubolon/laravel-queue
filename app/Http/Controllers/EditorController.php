<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Events\PublishBlogEvents;
use RealRashid\SweetAlert\Facades\Alert;

class EditorController extends Controller
{
    public function index()
    {
        $blogs = Blog::where('publish', 0)->get();
        return view('editor.index', compact('blogs'));
    }

    public function publish($id)
    {
        $blog = Blog::where('id', $id)->first();
        $blog->update([
            'publish'   => 1
        ]);

        event(new PublishBlogEvents($blog));
        Alert::success('Berhasil', 'Blog berhasil di publish');
        return redirect('editor');
    }
}
