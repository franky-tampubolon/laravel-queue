<?php

namespace App\Listeners;

use App\Events\BlogEvents;
use App\Mail\BlogPostMail;
use App\Mail\EditorBlogMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class BlogMailListener implements ShouldQueue
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BlogEvents  $event
     * @return void
     */
    public function handle(BlogEvents $event)
    {
        Mail::to($event->blog->user->email)->send(new BlogPostMail($event->blog));
        Mail::to('editor@gmail.com')->send(new EditorBlogMail($event->blog));
    }
}
