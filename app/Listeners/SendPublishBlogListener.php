<?php

namespace App\Listeners;

use App\Events\BlogEvents;
use App\Events\PublishBlogEvents;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendPublishBlogMail;

class SendPublishBlogListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BlogEvents  $event
     * @return void
     */
    public function handle(PublishBlogEvents $event)
    {
        Mail::to($event->blog->user->email)->send(new SendPublishBlogMail($event->blog));
    }
}
