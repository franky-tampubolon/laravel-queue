<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('auth')->group(function(){
    Route::get('user', 'UserController@index');
    Route::get('user/create', 'UserController@create');
    Route::post('user', 'UserController@store');
    Route::get('user/{user}', 'UserController@edit');
    
    Route::get('blog', 'BlogController@index');
    Route::get('blog/create', 'BlogController@create');
    Route::post('blog', 'BlogController@store');
    Route::get('role', 'RoleController@index');
    Route::get('role/create', 'RoleController@create');
    Route::post('role', 'RoleController@store');


    Route::get('editor', 'EditorController@index');
    Route::patch('editor/update/{blog}', 'EditorController@publish');
});